<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Animal Name : $sheep->name <br>"; // "shaun"
echo "Legs : $sheep->legs <br>"; // 2
echo "Cold Blooded : $sheep->cold_blooded <br>"; // false

$kodok = new Frog("buduk"); echo "<br>";
$kodok->jump() ;  // "hop hop"

echo "Animal Name : $kodok->name <br>";
echo "Legs : $kodok->legs <br>";

$sungokong = new Ape("kera sakti"); echo "<br>";
$sungokong->yell() ; // "Auooo"

echo "Animal Name : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br>";

?>